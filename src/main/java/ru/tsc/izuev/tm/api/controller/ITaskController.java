package ru.tsc.izuev.tm.api.controller;

public interface ITaskController {

    void createTask();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void changeTaskById();

    void changeTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

}
